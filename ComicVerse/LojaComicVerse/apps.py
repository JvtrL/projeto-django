from django.apps import AppConfig


class LojacomicverseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'LojaComicVerse'
